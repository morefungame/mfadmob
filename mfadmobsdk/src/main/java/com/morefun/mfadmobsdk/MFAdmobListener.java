package com.morefun.mfadmobsdk;

public interface MFAdmobListener {

    void onInitializationComplete();

    void onAdLoaded();

    void onAdFailedToLoad(String message);

    void onAdShowed();

    void onAdFailedToShow(String message);

    void onAdDismissed();

    void onUserEarnedReward();
}
