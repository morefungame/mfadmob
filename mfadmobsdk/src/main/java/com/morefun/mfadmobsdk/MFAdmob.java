package com.morefun.mfadmobsdk;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.AdapterStatus;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

import java.util.Arrays;
import java.util.Map;

public class MFAdmob {
    private final static  String TAG = "MFAdmob";
    public static String mUnitId = "";
    private static final String testUnitId = "ca-app-pub-3940256099942544/5224354917";
    public static int DELAY = 20 * 1000;
    private static RewardedAd mRewardedAd;
    private static Activity mActivity;
    private static MFAdmobListener mListener;
    private static boolean isLoading;

    @SuppressLint("HandlerLeak")
    private static Handler handler;

    public static void initialize(final Activity activity, String unitId, MFAdmobListener listener){
        mActivity = activity;
        mUnitId = unitId;
        mListener = listener;
        handler = new Handler(activity.getMainLooper()){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if (!isLoading)
                    loadRewardAds(mActivity);
            }
        };
        MobileAds.initialize(activity, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(@NonNull InitializationStatus initializationStatus) {
                if (initializationStatus.getAdapterStatusMap().size() > 0){
                   for (Map.Entry<String, AdapterStatus> entry : initializationStatus.getAdapterStatusMap().entrySet()){
                       Log.i(TAG, entry.getKey() + ":" + entry.getValue().getInitializationState());
                   }
                }
                Log.i(TAG, "InitializationStatus" + initializationStatus.getAdapterStatusMap());
                loadRewardAds(activity);
                if (mListener != null){
                    mListener.onInitializationComplete();
                }
            }
        });
    }

    private static void loadRewardAds(Activity activity){
        isLoading = true;
        AdRequest adRequest = new AdRequest.Builder().build();
        RewardedAd.load(activity, mUnitId, adRequest, new RewardedAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                super.onAdLoaded(rewardedAd);
                Log.i(TAG, "onAdLoaded");
                mRewardedAd = rewardedAd;
                if (mListener != null){
                    mListener.onAdLoaded();
                }
                isLoading = false;
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Log.i(TAG, "onAdFailedToLoad:" + loadAdError.getMessage());
                mRewardedAd = null;
                if (mListener != null){
                    mListener.onAdFailedToLoad(loadAdError.getMessage());
                }
                isLoading = false;
                loadAdDelay();
            }
        });
    }

    public static void show(final Activity activity){
        if (mRewardedAd != null){
            mRewardedAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                @Override
                public void onAdShowedFullScreenContent() {
                    // Called when ad is shown.
                    Log.d(TAG, "Ad was shown.");
                    if (mListener != null){
                        mListener.onAdShowed();
                    }
                }

                @Override
                public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                    // Called when ad fails to show.
                    Log.d(TAG, "Ad failed to show.");
                    if (mListener != null){
                        mListener.onAdFailedToShow(adError.getMessage());
                    }
                }

                @Override
                public void onAdDismissedFullScreenContent() {
                    // Called when ad is dismissed.
                    // Set the ad reference to null so you don't show the ad a second time.
                    Log.d(TAG, "Ad was dismissed.");
                    mRewardedAd = null;
                    if (mListener != null){
                        mListener.onAdDismissed();
                    }
                    loadRewardAds(activity);
                }
            });
            mRewardedAd.show(activity, new OnUserEarnedRewardListener() {
                @Override
                public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                    Log.d(TAG, "The user earned the reward.");
                    int rewardAmount = rewardItem.getAmount();
                    String rewardType = rewardItem.getType();
                    if (mListener != null){
                        mListener.onUserEarnedReward();
                    }
                    loadRewardAds(activity);
                }
            });
        }
    }

    private static void loadAdDelay(){
        handler.sendEmptyMessageDelayed(0, DELAY );
    }

    public static void setTestDevice(@NonNull String deviceId){
        RequestConfiguration configuration = new RequestConfiguration.Builder().setTestDeviceIds(Arrays.asList(deviceId)).build();
        MobileAds.setRequestConfiguration(configuration);
    }
}
