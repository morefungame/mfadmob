package com.morefun.mfadmob;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.RewardedVideoAd;
import com.facebook.ads.RewardedVideoAdListener;
import com.google.android.ads.mediationtestsuite.MediationTestSuite;
import com.morefun.mfadmobsdk.MFAdmob;
import com.morefun.mfadmobsdk.MFAdmobListener;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = MainActivity.class.getSimpleName();
    private final static String UNIT_ID = "ca-app-pub-1195438634037476/4738918339";
    private RewardedVideoAd rewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MFAdmob.setTestDevice("9C76502E49F79D5155109F749629A260");
        MFAdmob.initialize(this, UNIT_ID, listener);
//        MediationTestSuite.launch(this);
        rewardedVideoAd = new RewardedVideoAd(this, UNIT_ID);
        RewardedVideoAdListener rewardedVideoAdListener = new RewardedVideoAdListener() {
            @Override
            public void onError(Ad ad, AdError error) {
                // Rewarded video ad failed to load
                Log.e(TAG, "Rewarded video ad failed to load: " + error.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Rewarded video ad is loaded and ready to be displayed
                Log.d(TAG, "Rewarded video ad is loaded and ready to be displayed!");
                rewardedVideoAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Rewarded video ad clicked
                Log.d(TAG, "Rewarded video ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Rewarded Video ad impression - the event will fire when the
                // video starts playing
                Log.d(TAG, "Rewarded video ad impression logged!");
            }

            @Override
            public void onRewardedVideoCompleted() {
                // Rewarded Video View Complete - the video has been played to the end.
                // You can use this event to initialize your reward
                Log.d(TAG, "Rewarded video completed!");

                // Call method to give reward
                // giveReward();
            }

            @Override
            public void onRewardedVideoClosed() {
                // The Rewarded Video ad was closed - this can occur during the video
                // by closing the app, or closing the end card.
                Log.d(TAG, "Rewarded video ad closed!");
            }
        };
        rewardedVideoAd.loadAd(
                rewardedVideoAd.buildLoadAdConfig()
                        .withAdListener(rewardedVideoAdListener)
                        .build());
    }

    private MFAdmobListener listener = new MFAdmobListener() {
        @Override
        public void onInitializationComplete() {

        }

        @Override
        public void onAdLoaded() {

        }

        @Override
        public void onAdFailedToLoad(String message) {

        }

        @Override
        public void onAdShowed() {

        }

        @Override
        public void onAdFailedToShow(String message) {

        }

        @Override
        public void onAdDismissed() {

        }

        @Override
        public void onUserEarnedReward() {

        }
    };

    public void show(View view){
        MFAdmob.show(this);
    }
}
